<?php

$_plugins['onload'][] = function()
{
    echo 'Echoer loaded!', PHP_EOL;
};

$_plugins['onforget'][] = function()
{
    echo 'Echoer being forgotten!', PHP_EOL;
};

$_plugins['oninput'][] = function($id, $input)
{
    echo '[', $id, '] -> ', $input, PHP_EOL;
};

$_plugins['onoutput'][] = function($id, $output)
{
    echo '[', $id, '] <- ', rtrim($output), PHP_EOL;
};

$_plugins['onconnect'][] = function($id, $host, $port)
{
    echo 'Connected to ', $host, ':', $port, ' as ', $id, '.', PHP_EOL; 
};

$_plugins['ondisconnect'][] = function($id)
{
    echo 'Disconnecting from ', $id, '...', PHP_EOL;
};
