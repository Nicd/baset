<?php

$_plugins['oninput'][] = function($id, $input) use (&$_write)
{
    if (substr($input, 0, 4) === 'PING')
    {
        $_write($id, 'PONG :' . substr($input, 6) . "\r\n");
    }
};
